var Parachute = function()
{
    this.canvas = document.getElementById('game-canvas'),
    this.context = this.canvas.getContext('2d'),
    this.fpsElement = document.getElementById('fps'),
    this.gameStarted = false,
    this.scoreElement = document.getElementById('score'),
    this.score=0;
    //Direction
    this.LEFT = 1,
    this.RIGHT = 2,
    //Constants 
    this.TRANSPARENT = 0,
    this.OPAQUE = 1.0,
    //Scrolling constants
    this.BACKGROUND_VELOCITY = 42,
    this.GRAVITY = 30,
    this.STARTING_BACKGROUND_VELOCITY = 0,
    this.STARTING_BACKGROUND_OFFSET = 0,
    this.spritesheet = new Image(),
    this.spriteOffset = 0,
    this.lastAdvanceTime = 0,
    this.RUN_ANIMATION_RATE = 10,
    //Background width and height
    this.BACKGROUND_WIDTH = 499,
    this.BACKGROUND_HEIGHT = 299,
    //Loading screen
    this.loadingElement = document.getElementById('loading'),
    this.loadingTitleElement = document.getElementById('loading-title'),
    this.snailAnimatedGifElement = document.getElementById('loading-animated-gif'),
    //Toast
    this.toastElement = document.getElementById('toast'),
    //Instructions
    this.instructionsElement = document.getElementById('instructions'),
    //Copyright
    this.copyrightElement = document.getElementById('copyright'),
    //Sound and music
    this.soundAndMusicElement = document.getElementById('sound-and-music'),
    //States
    this.paused = false,
    this.PAUSED_CHECK_INTERVAL = 200,
    this.windowHasFocus = true,
    this.countdownInProgress = false,
    //Time
    this.lastAnimationFrameTime = 0,
    this.fps = 60,
    //Position Changes
    this.bgVelocityX = this.STARTING_BACKGROUND_VELOCITY,
    this.bgVelocityY = this.STARTING_BACKGROUND_VELOCITY,
    this.backgroundOffsetX = this.STARTING_BACKGROUND_OFFSET,
    this.backgroundOffsetY = this.STARTING_BACKGROUND_OFFSET,
    //Player Changes
    this.pVelocityY = 0,
    this.playerOffsetY = 0,
    this.playerOffsetX = 0,
    //Sprite Changes
    this.spriteOffsetY=0;
    this.spriteOffsetX=0;
    this.spriteVelocityX=0;
    this.spriteVelocityY=0;
    this.lastFpsUpdateTime = 0,
    this.pauseStartTime;
    
    //Spritesheet Cells
    this.PLANE_HEIGHT = 62;
    this.PLANE_WIDTH = 113;
    
    this.SEAGULL_DOWN_HEIGHT = 21;
    this.SEAGULL_DOWN_WIDTH = 32;
    
    this.SEAGULL_LEFT_HEIGHT = 27;
    this.SEAGULL_LEFT_WIDTH = 21;
    
    this.SEAGULL_RIGHT_HEIGHT = 27;
    this.SEAGULL_RIGHT_WIDTH = 21;
    
    this.SEAGULL_UP_HEIGHT = 21;
    this.SEAGULL_UP_WIDTH = 32;
    
    this.CROW_DOWN_HEIGHT = 23;
    this.CROW_DOWN_WIDTH = 32;
    
    this.CROW_LEFT_HEIGHT = 29;
    this.CROW_LEFT_WIDTH = 23;
    
    this.CROW_RIGHT_HEIGHT = 29;
    this.CROW_RIGHT_WIDTH = 23;
    
    this.CROW_UP_HEIGHT = 19;
    this.CROW_UP_WIDTH = 32;
    
    this.SOLDIER_UP_HEIGHT = 26;
    this.SOLDIER_UP_WIDTH = 16;
    
    this.SOLDIER_RIGHT_HEIGHT = 27;
    this.SOLDIER_RIGHT_WIDTH = 17;
    
    this.SOLDIER_DOWN_HEIGHT = 26;
    this.SOLDIER__WIDTH = 16;
    
    this.SOLDIER_RIGHT_HEIGHT = 27;
    this.SOLDIER_RIGHT_WIDTH = 17;
    
    this.PARACHUTE_HEIGHT = 68;
    this.PARACHUTE_WIDTH = 100;
    
    this.EMPTY_PARACHUTE_HEIGHT = 60;
    this.EMPTY_PARACHUTE_Width = 100;
    
    this.bluePlane1Cells = [
        {left: 384, top:310, width:this.PLANE_WIDTH, height:this.PLANE_HEIGHT},
        {left: 262, top:310, width:this.PLANE_WIDTH, height:this.PLANE_HEIGHT},
        {left: 140, top:310, width:this.PLANE_WIDTH, height:this.PLANE_HEIGHT},
        {left: 18,  top:310, width:this.PLANE_WIDTH, height:this.PLANE_HEIGHT}
    ];
    
    this.bluePlane2Cells = [
        {left: 384, top:382, width:this.PLANE_WIDTH, height:this.PLANE_HEIGHT},
        {left: 262, top:382, width:this.PLANE_WIDTH, height:this.PLANE_HEIGHT},
        {left: 140, top:382, width:this.PLANE_WIDTH, height:this.PLANE_HEIGHT},
        {left: 18,  top:382, width:this.PLANE_WIDTH, height:this.PLANE_HEIGHT}
    ];
    
    this.orangePlane1Cells = [
        {left: 384, top:457, width:this.PLANE_WIDTH, height:this.PLANE_HEIGHT},
        {left: 262, top:457, width:this.PLANE_WIDTH, height:this.PLANE_HEIGHT},
        {left: 140, top:457, width:this.PLANE_WIDTH, height:this.PLANE_HEIGHT},
        {left: 18,  top:457, width:this.PLANE_WIDTH, height:this.PLANE_HEIGHT}
    ];
    
    this.orangePlane2Cells = [
        {left: 384, top:529, width:this.PLANE_WIDTH, height:this.PLANE_HEIGHT},
        {left: 262, top:529, width:this.PLANE_WIDTH, height:this.PLANE_HEIGHT},
        {left: 140, top:529, width:this.PLANE_WIDTH, height:this.PLANE_HEIGHT},
        {left: 18,  top:529, width:this.PLANE_WIDTH, height:this.PLANE_HEIGHT}
    ];
    
    this.seagullUpCells = [
        {left: 2,  top:1015, width:this.SEAGULL_UP_WIDTH, height:this.SEAGULL_UP_HEIGHT},
        {left: 42, top:1015, width:this.SEAGULL_UP_WIDTH, height:this.SEAGULL_UP_HEIGHT},
        {left: 80, top:1015, width:this.SEAGULL_UP_WIDTH, height:this.SEAGULL_UP_HEIGHT}
    ];
    
    this.seagullDownCells = [
        {left: 2,  top:830, width:this.SEAGULL_DOWN_WIDTH, height:this.SEAGULL_DOWN_HEIGHT},
        {left: 35, top:830, width:this.SEAGULL_DOWN_WIDTH, height:this.SEAGULL_DOWN_HEIGHT},
        {left: 68, top:830, width:this.SEAGULL_DOWN_WIDTH, height:this.SEAGULL_DOWN_HEIGHT}
    ];
    
    this.seagullLeftCells = [
        {left: 4,  top:889, width:this.SEAGULL_LEFT_WIDTH, height:this.SEAGULL_LEFT_HEIGHT},
        {left: 36, top:888, width:this.SEAGULL_LEFT_WIDTH, height:this.SEAGULL_LEFT_HEIGHT},
        {left: 68, top:887, width:this.SEAGULL_LEFT_WIDTH, height:this.SEAGULL_LEFT_HEIGHT}
    ];
    
    this.seagullRightCells = [
        {left: 11,  top:953, width:this.SEAGULL_UP_WIDTH, height:this.SEAGULL_RIGHT_HEIGHT},
        {left: 43, top:952, width:this.SEAGULL_UP_WIDTH, height:this.SEAGULL_RIGHT_HEIGHT},
        {left: 75, top:951, width:this.SEAGULL_UP_WIDTH, height:this.SEAGULL_RIGHT_HEIGHT}
    ];
    
    this.parachuteCells = [
        {left:77, top:1046, width:this.PARACHUTE_WIDTH, height:this.PARACHUTE_HEIGHT},
        {left:184, top:1044, width:this.PARACHUTE_WIDTH, height:this.PARACHUTE_HEIGHT},
        {left:78, top:1117, width:this.PARACHUTE_WIDTH, height:this.PARACHUTE_HEIGHT}
    ];
    
    this.orangePlaneData = [];
    this.bluePlaneData = [];
    this.seagullLeftData = [
        {left:250, top:400},
        {left:500, top:300},
        {left:750, top:550},
        {left:220, top:850},
        {left:480, top:680},
        {left:730, top:750}
    ];
    
    this.seagullDownData = [
        {left:200, top:350},
        {left:550, top:100},
        {left:600, top:400}
    ];
    
    this.seagullRightData = [
        {left:400, top:400},
        {left:100, top:100},
        {left:600, top:200},
        {left:300, top:700}
    ];
    
    this.seagullUpData = [
        {left:150, top:600},
        {left:850, top:300},
        {left:650, top:200}
    ];
    
    
    this.leftSeagulls = [];
    this.rightSeagulls = [];
    this.upSeagulls = [];
    this.downSeagulls = [];
    this.orangePlanes = [];
    this.bluePlanes = [];
    this.sprites = [];
    
    this.parachuteBehaviour =
    {
        lastAdvanceTime: 0,
        execute: function (sprite, now, fps, context,lastAnimationFrameTime)
        {
           
            if (this.lastAdvanceTime === 0)
                this.lastAdvanceTime = now;
            else if (now - this.lastAdvanceTime >1000 / sprite.runAnimationRate)
            {
                sprite.artist.advance();
                this.lastAdvanceTime = now;
            }
        }
    };
};

Parachute.prototype = 
{
    createSprites: function ()
    {
        this.createParachuteSprite();
        this.createSeagullLeftSprites();
        this.createSeagullRightSprites();
        this.createSeagullUpSprites();
        this.createSeagullDownSprites();
        this.initializeSprites();
        this.addSpritesToSpriteArray();
    },
    
    positionSprites: function (sprites, spriteData) {
        var sprite;

        for (var i = 0; i < sprites.length; ++i) {
            sprite = sprites[i];
            sprite.top = spriteData[i].top;
            sprite.left = spriteData[i].left;
        }
    },
    
    initializeSprites: function () 
    {
        this.positionSprites(this.leftSeagulls, this.seagullLeftData);
        this.positionSprites(this.rightSeagulls, this.seagullRightData);
        this.positionSprites(this.upSeagulls, this.seagullUpData);
        this.positionSprites(this.downSeagulls, this.seagullDownData);
    },
    initializeImages: function()
    {
        this.spritesheet.src = 'images/spritesheet.png';
        this.spritesheet.onload = function (e)
        {
            parachute.backgroundLoaded();
        };
    },
    backgroundLoaded: function ()
    {
        var LOADIND_SCREEN_TRANSITION_DURATION = 2000;
        this.fadeOutElements(this.loadingElement, LOADIND_SCREEN_TRANSITION_DURATION);

        setTimeout(function () {
            parachute.startGame();
            parachute.gameStarted = true;
        }, LOADIND_SCREEN_TRANSITION_DURATION);
    },
    drawBackground: function ()
    {
        var BACKGROUND_TOP_IN_SPRITESHEET = 0;
        this.context.translate(-this.backgroundOffsetX,-this.backgroundOffsetY);
        //First Row
        //Initially Onscreen left
        this.context.drawImage(this.spritesheet, 0, BACKGROUND_TOP_IN_SPRITESHEET,
        this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT, 0, 0, this.BACKGROUND_WIDTH,
        this.BACKGROUND_HEIGHT);
        //Initially Onscreen right
        this.context.drawImage(this.spritesheet, 0, BACKGROUND_TOP_IN_SPRITESHEET,
        this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT, this.BACKGROUND_WIDTH, 0, this.BACKGROUND_WIDTH,
        this.BACKGROUND_HEIGHT);
        
        //Second Row
        //Initially Onscreen left
        this.context.drawImage(this.spritesheet, 0, BACKGROUND_TOP_IN_SPRITESHEET,
        this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT, 0, this.BACKGROUND_HEIGHT, this.BACKGROUND_WIDTH,
        this.BACKGROUND_HEIGHT);
        //Initially Onscreen right
        this.context.drawImage(this.spritesheet, 0, BACKGROUND_TOP_IN_SPRITESHEET,
        this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT, this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT, this.BACKGROUND_WIDTH,
        this.BACKGROUND_HEIGHT);
        
        //Third Row
        //Initially Onscreen left
        this.context.drawImage(this.spritesheet, 0, BACKGROUND_TOP_IN_SPRITESHEET,
        this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT, 0, 2*this.BACKGROUND_HEIGHT, this.BACKGROUND_WIDTH,
        this.BACKGROUND_HEIGHT);
        //Initially Onscreen right
        this.context.drawImage(this.spritesheet, 0, BACKGROUND_TOP_IN_SPRITESHEET,
        this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT, this.BACKGROUND_WIDTH, 2*this.BACKGROUND_HEIGHT, this.BACKGROUND_WIDTH,
        this.BACKGROUND_HEIGHT);
        
        //Fourth Row
        //Initially Onscreen left
        this.context.drawImage(this.spritesheet, 0, BACKGROUND_TOP_IN_SPRITESHEET,
        this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT, 0, 3*this.BACKGROUND_HEIGHT, this.BACKGROUND_WIDTH,
        this.BACKGROUND_HEIGHT);
        //Initially Onscreen right
        this.context.drawImage(this.spritesheet, 0, BACKGROUND_TOP_IN_SPRITESHEET,
        this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT, this.BACKGROUND_WIDTH, 3*this.BACKGROUND_HEIGHT, this.BACKGROUND_WIDTH,
        this.BACKGROUND_HEIGHT);
        
        //Ground Row
        //Initially Onscreen left
        this.context.drawImage(this.spritesheet, 510, BACKGROUND_TOP_IN_SPRITESHEET,
        this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT, 0, 3*this.BACKGROUND_HEIGHT, this.BACKGROUND_WIDTH,
        this.BACKGROUND_HEIGHT);
        //Initially Onscreen right
        this.context.drawImage(this.spritesheet, 510, BACKGROUND_TOP_IN_SPRITESHEET,
        this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT, this.BACKGROUND_WIDTH, 3*this.BACKGROUND_HEIGHT, this.BACKGROUND_WIDTH,
        this.BACKGROUND_HEIGHT);
        this.context.translate(this.backgroundOffsetX, this.backgroundOffsetY);
        
    },
    dimControls: function ()
    {
        FINAL_OPACITY = 0.5;
        parachute.instructionsElement.style.opacity = FINAL_OPACITY;
        parachute.soundAndMusicElement.style.opacity = FINAL_OPACITY;
    },
    revealCanvas: function ()
    {
        this.fadeInElements(this.canvas);
    },
    revealTopChrome: function ()
    {
        this.fadeInElements(this.fpsElement, this.scoreElement);
    },
    revealTopChromeDimmed: function ()
    {
        var DIM = 0.25;

        this.scoreElement.style.display = 'block';
        this.fpsElement.style.display = 'block';

        setTimeout(function ()
        {
            parachute.scoreElement.style.opacity = DIM;
            parachute.fpsElement.style.opacity = DIM;
        }, this.SHORT_DELAY);
    },
    revealBottomChrome: function ()
    {
        this.fadeInElements(this.soundAndMusicElement,
                this.instructionsElement, this.copyrightElement);
    },
    revealGame: function ()
    {
        var DIM_CONTROLS_DELAY = 5000;

        this.revealTopChromeDimmed();
        this.revealCanvas();
        this.revealBottomChrome();

        setTimeout(function ()
        {
            parachute.dimControls();
            parachute.revealTopChrome();
        }, DIM_CONTROLS_DELAY);
    },
    revealInitialToast: function ()
    {
        var INITIAL_TOAST_DELAY = 1500,
                INITIAL_TOAST_DURATION = 3000;

        setTimeout(function ()
        {
            parachute.revealToast('Avoid Objects as you fall', INITIAL_TOAST_DELAY);
        }, INITIAL_TOAST_DURATION);
    },
    revealToast: function (text, duration) {
        var DEFAULT_TOAST_DISPLAY_DURATION = 1000;
        duration = duration || DEFAULT_TOAST_DISPLAY_DURATION;

        this.startToastTransition(text, duration);

        setTimeout(function (e)
        {

            parachute.hideToast();

        }, duration);
    },
    startToastTransition: function (text, duration) {
        this.toastElement.innerHTML = text;
        this.fadeInElements(this.toastElement);
    },
    hideToast: function ()
    {
        var TOAST_TRANSITION_DURATION = 450;
        this.fadeOutElements(this.toastElement, TOAST_TRANSITION_DURATION);
    },
    startGame: function()
    {
        this.revealGame();
        this.revealInitialToast();
        window.requestAnimationFrame(this.animate);
        this.down();
    },
    
    
    animate: function (now)
    {
        if (parachute.paused)
        {
            setTimeout(function ()
            {
                requestAnimationFrame(parachute.animate);
            }, parachute.PAUSED_CHECK_INTERVAL);
        }
        else
        {
            parachute.fps = parachute.calculateFps(now);
            parachute.draw(now);
            parachute.lastAnimationFrameTime = now;
            requestAnimationFrame(parachute.animate);
        }
    },
    
    draw: function (now)
    {
        this.setOffsets(now);
        this.drawBackground();
        this.updateSprites(now);
        this.drawSprites();
    },
    
    togglePaused: function () {
        var now = +new Date();
        this.paused = !this.paused;
        if (this.paused)
        {
            this.pauseStartTime = now;
        }
        else
        {
            this.lastAnimationFrameTime += (now - this.pauseStartTime);
        }
    },
    
    
    setOffsets: function (now)
    {
        this.setBackgroundOffset(now);
        this.setPlayerOffset(now);
        this.setSpriteOffset(now);
    },
    
    setBackgroundOffset: function (now)
    {
        this.backgroundOffsetX += this.bgVelocityX * (now - this.lastAnimationFrameTime) / 1000;
        if (this.backgroundOffsetX < 0)
        {
            this.bgVelocityX = this.BACKGROUND_VELOCITY;
        }
        else if(this.backgroundOffsetX > (this.BACKGROUND_WIDTH*2)-500)
        {
            this.bgVelocityX = -this.BACKGROUND_VELOCITY;
        }
        
        this.backgroundOffsetY += this.bgVelocityY * (now - this.lastAnimationFrameTime) / 1000;
        if (this.backgroundOffsetY < 0)
        {
            this.bgVelocityY = this.BACKGROUND_VELOCITY;
        }
        else if(this.backgroundOffsetY > (this.BACKGROUND_HEIGHT*5)-1000)
        {
            this.bgVelocityY = 0;
            this.bgVelocityX = 0;
            this.backgroundOffsetY = (this.BACKGROUND_HEIGHT*5)-1000;
        }
    },
    
    setPlayerOffset: function (now)
    {
        this.playerOffsetY += this.pVelocityY * (now - this.lastAnimationFrameTime) / 1000;
        if (this.playerOffsetY < -450+this.PARACHUTE_HEIGHT/2)
        {
            this.playerOffsetY = -450+this.PARACHUTE_HEIGHT/2;
        }
        else if(this.playerOffsetY > this.BACKGROUND_HEIGHT*4.5-this.PARACHUTE_HEIGHT)
        {
            //this.playerOffsetY = this.BACKGROUND_HEIGHT*4.5;
        }
        
        this.parachute_player.OffsetX = this.playerOffsetX;
        this.parachute_player.OffsetY = this.playerOffsetY;
    },
    setSpriteOffset: function (now)
    {
        var sprite;
        this.spriteOffsetY += this.bgVelocityY*(now - this.lastAnimationFrameTime) / 1000;
        this.spriteOffsetX += this.bgVelocityX*(now - this.lastAnimationFrameTime) / 1000;
        
        
        for (var i = 0; i < this.sprites.length; ++i)
        {
            sprite = this.sprites[i];
            if ('player' !== sprite.type)
            {
                sprite.OffsetX = this.spriteOffsetX;
                sprite.OffsetY = this.spriteOffsetY;
            }
        }
    },
    calculateFps: function (now)
    {
        var fps = 1 / (now - this.lastAnimationFrameTime) * 1000;
        if (now - this.lastFpsUpdateTime > 1000)
        {
            this.lastFpsUpdateTime = now;
            this.fpsElement.innerHTML = fps.toFixed(0) + 'fps';
        }
        return fps;
    },
    
    fadeInElements: function ()
    {
        var args = arguments;
        for (var i = 0; i < args.length; ++i)
        {
            console.log(args[i]);
            args[i].style.display = 'block';
        }

        setTimeout(function () {
            for (var i = 0; i < args.length; ++i)
            {
                args[i].style.opacity = parachute.OPAQUE;
            }
        }, this.SHORT_DELAY);
    },
    
    fadeOutElements: function ()
    {
        var args = arguments,
                fadeDuration = args[args.length - 1];
        for (var i = 0; i < args.length - 1; ++i)
        {
            console.log(args[i]);
            args[i].style.opacity = parachute.TRANSPARENT;
        }

        setTimeout(function () {
            for (var i = 0; i < args.length - 1; ++i)
            {
                args[i].style.display = 'none';
            }
        }, fadeDuration);
    },
    left: function ()
    {
        this.bgVelocityX = -this.BACKGROUND_VELOCITY;
    },
    right: function ()
    {
        this.bgVelocityX = this.BACKGROUND_VELOCITY;
    },
    
    up: function ()
    {
        this.bgVelocityY = -2*this.GRAVITY;
        this.pVelocityY = 1.6*this.GRAVITY;
        setTimeout(function (e)
        {
            parachute.pVelocityY = -parachute.GRAVITY*0.8;
            parachute.bgVelocityY =  parachute.GRAVITY;
            },500);
        this.score++;
        document.getElementById('score').innerHTML = this.score;
    },
    down: function ()
    {
        this.pVelocityY = -this.GRAVITY*0.8;
        this.bgVelocityY = this.GRAVITY;
    },
    
    createParachuteSprite: function()
    {
        var STARTING_LEFT = 250-this.PARACHUTE_WIDTH/2,
            STARTING_TOP  = 150-this.PARACHUTE_HEIGHT/2,
            STARTING_ANIMATION_RATE = 0;
    
            this.parachute_player = new Sprite('player', new SpriteSheetArtist(this.spritesheet, this.parachuteCells), [this.parachuteBehaviour]);
            this.parachute_player.runAnimationRate =  STARTING_ANIMATION_RATE;
            this.parachute_player.left = STARTING_LEFT;
            this.parachute_player.top = STARTING_TOP;
    },
    
    createSeagullLeftSprites: function()
    {
      var seagull;
      var SEAGULL_FLAP_DURATION = 50;
      var SEAGULL_FLAP_INTERVAL = 0;
      for (var i = 0; i < this.seagullLeftData.length; ++i)
        {
            seagull = new Sprite('seagull', new SpriteSheetArtist(this.spritesheet, this.seagullLeftCells), [new CycleBehaviour(SEAGULL_FLAP_DURATION, SEAGULL_FLAP_INTERVAL)]);
            seagull.width = this.SEAGULL_LEFT_WIDTH;
            seagull.height = this.SEAGULL_LEFT_HEIGHT;
            
            this.leftSeagulls.push(seagull);
        }
    },
    
    createSeagullRightSprites: function()
    {
      var seagull;
      var SEAGULL_FLAP_DURATION = 50;
      var SEAGULL_FLAP_INTERVAL = 0;
      for (var i = 0; i < this.seagullRightData.length; ++i)
        {
            seagull = new Sprite('seagull', new SpriteSheetArtist(this.spritesheet, this.seagullRightCells), [new CycleBehaviour(SEAGULL_FLAP_DURATION, SEAGULL_FLAP_INTERVAL)]);
            seagull.width = this.SEAGULL_LEFT_WIDTH;
            seagull.height = this.SEAGULL_LEFT_HEIGHT;
            
            this.rightSeagulls.push(seagull);
        }
    },
    
    createSeagullUpSprites: function()
    {
      var seagull;
      var SEAGULL_FLAP_DURATION = 50;
      var SEAGULL_FLAP_INTERVAL = 0;
      for (var i = 0; i < this.seagullUpData.length; ++i)
        {
            seagull = new Sprite('seagull', new SpriteSheetArtist(this.spritesheet, this.seagullUpCells), [new CycleBehaviour(SEAGULL_FLAP_DURATION, SEAGULL_FLAP_INTERVAL)]);
            seagull.width = this.SEAGULL_LEFT_WIDTH;
            seagull.height = this.SEAGULL_LEFT_HEIGHT;
            
            this.upSeagulls.push(seagull);
        }
    },
    
    createSeagullDownSprites: function()
    {
      var seagull;
      var SEAGULL_FLAP_DURATION = 50;
      var SEAGULL_FLAP_INTERVAL = 0;
      for (var i = 0; i < this.seagullDownData.length; ++i)
        {
            seagull = new Sprite('seagull', new SpriteSheetArtist(this.spritesheet, this.seagullDownCells), [new CycleBehaviour(SEAGULL_FLAP_DURATION, SEAGULL_FLAP_INTERVAL)]);
            seagull.width = this.SEAGULL_LEFT_WIDTH;
            seagull.height = this.SEAGULL_LEFT_HEIGHT;
            
            this.downSeagulls.push(seagull);
        }
    },
    
    addSpritesToSpriteArray: function ()
    {
        this.sprites.push(this.parachute_player);
        for (var i = 0; i < this.leftSeagulls.length; ++i)
        {
            this.sprites.push(this.leftSeagulls[i]);
        }
        for (var i = 0; i < this.rightSeagulls.length; ++i)
        {
            this.sprites.push(this.rightSeagulls[i]);
        }
        for (var i = 0; i < this.upSeagulls.length; ++i)
        {
            this.sprites.push(this.upSeagulls[i]);
        }
        for (var i = 0; i < this.downSeagulls.length; ++i)
        {
            this.sprites.push(this.downSeagulls[i]);
        }
    },
    
    
    
    
    isSpriteInView: function (sprite)
    {
        //only draw sprite if visible on canvas
        return (sprite.left + sprite.width > sprite.OffsetX && sprite.left < sprite.OffsetX + this.canvas.width)
                ||(sprite.top + sprite.height > sprite.OffsetY && sprite.top < sprite.OffsetY + this.canvas.height);
    },
    //Update all sprites first before drawing 
    //all sprites
    updateSprites: function (now)
    {
        var sprite;
        for (var i = 0; i < this.sprites.length; ++i)
        {
            sprite = this.sprites[i];
            if (sprite.visible && this.isSpriteInView(sprite))
            {
                sprite.update(now, this.fps, this.context,
                        this.lastAnimationFrameTime);
            }
        }
    },
    drawSprites: function ()
    {
        var sprite;
        for (var i = 0; i < this.sprites.length; ++i)
        {
            sprite = this.sprites[i];
            if (sprite.visible && this.isSpriteInView(sprite))
            {
                this.context.translate(-sprite.OffsetX, -sprite.OffsetY);
                sprite.draw(this.context);
                this.context.translate(sprite.OffsetX, sprite.OffsetY);
            }
        }
    }
};

//add some vars for keycodes
var KEY_LEFT_ARROW = 37,
    KEY_RIGHT_ARROW = 39,
    KEY_UP_ARROW = 38,
    KEY_DOWN_ARROW = 40,
    KEY_SPACE = 32,
    KEY_P = 80;

//Add Event Listener
window.addEventListener('keydown', function (e) {
    var key = e.keyCode;
    if (key === KEY_LEFT_ARROW)
    {
        parachute.left();
    }
    else if (key === KEY_RIGHT_ARROW)
    {
        parachute.right();
    }
    else if (key === KEY_SPACE)
    {
        parachute.up();
    }
    else if (key === KEY_P)
    {
        parachute.togglePaused();
    }
});

window.addEventListener('keyup', function (e) {
    var key = e.keyCode;
    if (key === KEY_LEFT_ARROW)
    {
        parachute.bgVelocityX=0;
    }
    else if (key === KEY_RIGHT_ARROW)
    {
        parachute.bgVelocityX=0;
    }
});
window.addEventListener('blur', function (e) {
    parachute.windowHasFocus = false;
    if (!parachute.paused)
    {
        parachute.togglePaused();
    }
});

window.addEventListener('focus', function (e) {
    var originalFont = parachute.toastElement.style.fontSize,
        DIGIT_DISPLAY_DURATION = 1000;
    parachute.windowHasFocus = true;
    parachute.countdownInProgress = true;
    if (parachute.paused)
    {
        parachute.toastElement.style.font = '128px Comic Sans MS';
        if (parachute.windowHasFocus && parachute.countdownInProgress)
            parachute.revealToast('3', DIGIT_DISPLAY_DURATION);
        setTimeout(function (e)
        {
            if (parachute.windowHasFocus && parachute.countdownInProgress)
                parachute.revealToast('2', DIGIT_DISPLAY_DURATION);

            setTimeout(function (e)
            {
                if (parachute.windowHasFocus && parachute.countdownInProgress)
                    parachute.revealToast('1', DIGIT_DISPLAY_DURATION);

                setTimeout(function (e)
                {
                    if (parachute.windowHasFocus && parachute.countdownInProgress)
                    {
                        parachute.togglePaused();
                    }
                    if (parachute.windowHasFocus && parachute.countdownInProgress)
                    {
                        parachute.toastElement.style.fontSize = originalFont;
                    }
                    parachute.countdownInProgress = false;
                }, DIGIT_DISPLAY_DURATION);
            }, DIGIT_DISPLAY_DURATION);
        }, DIGIT_DISPLAY_DURATION);
    }
});

var parachute = new Parachute();
parachute.initializeImages();
parachute.createSprites();

