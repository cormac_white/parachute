CycleBehaviour = function (duration, interval)
{
    this.duration = duration || 1000;
    this.lastAdvance = 0;
    this.interval = interval;
};

CycleBehaviour.prototype =
{
    execute: function (sprite, now, fps, context, lastAnimationFramTime)
    {
        if (this.lastAdvance === 0) //First time only
        {
            this.lastAdvance = now;
        }
        if (this.interval && sprite.artist.cellIndex === 0)
        {
            if (now - this.lastAdvance > this.interval)
            {
                sprite.artist.advance();
                this.lastAdvance = now;
            }
        }
        else if (now - this.lastAdvance > this.duration)
        {
            sprite.artist.advance();
            this.lastAdvance = now;
        }
    }
};